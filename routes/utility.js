var request = require('request');
var http = require("http");    
const parseString = require('xml2js').parseString;
var jsonxml = require('jsontoxml');


module.exports={
    xmlconvertion:function(data){
        var xml = jsonxml({
            bookingrequest:data.bookingrequest
        })
        return xml;
    },
    XmlData:function(inputData)
    {   

        var data = "<BOOKINGREQUEST>"
        const transid = inputData.bookingrequest.svcheader.transid;
        const userid = inputData.bookingrequest.svcheader.userid;
        const svcaction = inputData.bookingrequest.svcaction;
        const awb = inputData.bookingrequest.awb;
        const awbproduct = inputData.bookingrequest.awbproduct;
        const payment = inputData.bookingrequest.payment;
        data += "<SVCHEADER>";
        data += "<TRANSID>"+transid+"</TRANSID>"
        data += "<USERID>"+userid+"</USERID>"
        data += "</SVCHEADER>"

        data += "<SVCACTION>"+svcaction+"</SVCACTION>"

        data += "<AWB>"+awb+"</AWB>"

        data += "<AWBPRODUCT>"+awbproduct+"</AWBPRODUCT>"

        data += "<PAYMENT>"+payment+"</PAYMENT>"

        const origin_station = inputData.bookingrequest.routing.origin_station;
        const via_station = inputData.bookingrequest.routing.via_station;
        const dest_station = inputData.bookingrequest.routing.dest_station;   
        data += "<ROUTING>"
        data += "<ORIGIN_STATION>"+origin_station+"</ORIGIN_STATION>"
        data += "<VIA_STATION>"+via_station+"</VIA_STATION>"
        data += "<DEST_STATION>"+dest_station+"</DEST_STATION>"
        data += "</ROUTING>"
        const weightunit = inputData.bookingrequest.weightunit;
        const volumeunit = inputData.bookingrequest.volumeunit;
        data += "<WEIGHTUNIT>"+weightunit+"</WEIGHTUNIT>"
        data += "<VOLUMEUNIT>"+volumeunit+"</VOLUMEUNIT>"
        const shipper_partaccount = inputData.bookingrequest.shipper.partaccount;
        const shipper_partname = inputData.bookingrequest.shipper.partname;
        const shipper_station = inputData.bookingrequest.shipper.station;
        const shipper_address = inputData.bookingrequest.shipper.address;
        const shipper_city = inputData.bookingrequest.shipper.city;
        const shipper_state = inputData.bookingrequest.shipper.state;
        const shipper_country = inputData.bookingrequest.shipper.country;
        const shipper_postalcode = inputData.bookingrequest.shipper.postalcode;
        const shipper_phone = inputData.bookingrequest.shipper.phone;
        const shipper_fax = inputData.bookingrequest.shipper.fax;
        const shipper_email = inputData.bookingrequest.shipper.email;
        data += "<SHIPPER>"
        data += "<PARTACCOUNT>"+shipper_partaccount+"</PARTACCOUNT>"
        data += "<PARTNAME>"+shipper_partname+"</PARTNAME>"
        data += "<ADDRESS>"+shipper_address+"</ADDRESS>"
        data += "<STATION>"+shipper_station+"</STATION>"
        data += "<CITY>"+shipper_city+"</CITY>"
        data += "<STATE>"+shipper_state+"</STATE>"
        data += "<COUNTRY>"+shipper_country+"</COUNTRY>"
        data += "<POSTALCODE>"+shipper_postalcode+"</POSTALCODE>"
        data += "<PHONE>"+shipper_phone+"</PHONE>"
        data += "<FAX>"+shipper_fax+"</FAX>"
        data += "<EMAIL>"+shipper_email+"</EMAIL>"
        data += "</SHIPPER>"
        const consignee_partaccount = inputData.bookingrequest.consignee.partaccount;
        const consignee_partname = inputData.bookingrequest.consignee.partname;
        const consignee_address = inputData.bookingrequest.consignee.address;
        const consignee_station = inputData.bookingrequest.consignee.station;
        const consignee_city = inputData.bookingrequest.consignee.city;
        const consignee_state = inputData.bookingrequest.consignee.state;
        const consignee_country = inputData.bookingrequest.consignee.country;
        const consignee_postalcode = inputData.bookingrequest.consignee.postalcode;
        const consignee_phone = inputData.bookingrequest.consignee.phone;
        const consignee_fax = inputData.bookingrequest.consignee.fax;
        const consignee_email = inputData.bookingrequest.consignee.email;
        data += "<CONSIGNEE>"
        data += "<PARTACCOUNT>"+consignee_partaccount+"</PARTACCOUNT>";
        data += "<PARTNAME>"+consignee_partname+"</PARTNAME>"
        data += "<ADDRESS>"+consignee_address+"</ADDRESS>"
        data += "<STATION>"+consignee_station+"</STATION>"
        data += "<CITY>"+consignee_city+"</CITY>"
        data += "<STATE>"+consignee_state+"</STATE>"
        data += "<COUNTRY>"+consignee_country+"</COUNTRY>"
        data += "<POSTALCODE>"+consignee_postalcode+"</POSTALCODE>"
        data += "<PHONE>"+consignee_phone+"</PHONE>"
        data += "<FAX>"+consignee_fax+"</FAX>"
        data += "<EMAIL>"+consignee_email+"</EMAIL>"
        data += "</CONSIGNEE>"
        const awbagent_partaccount = inputData.bookingrequest.awbagent.partaccount;
        const awbagent_partname = inputData.bookingrequest.awbagent.partname;
        const awbagent_address = inputData.bookingrequest.awbagent.address;
        const awbagent_station = inputData.bookingrequest.awbagent.station;
        const awbagent_city = inputData.bookingrequest.awbagent.city;
        const awbagent_state = inputData.bookingrequest.awbagent.state;
        const awbagent_country = inputData.bookingrequest.awbagent.country;
        const awbagent_postalcode = inputData.bookingrequest.awbagent.postalcode;
        const awbagent_phone = inputData.bookingrequest.awbagent.phone;
        const awbagent_fax = inputData.bookingrequest.awbagent.fax;
        const awbagent_email = inputData.bookingrequest.awbagent.email;
        data += "<AWBAGENT>"
        data += "<PARTACCOUNT>"+awbagent_partaccount+"</PARTACCOUNT>"
        data += "<PARTNAME>"+awbagent_partname+"</PARTNAME>"
        data += "<ADDRESS>"+awbagent_address+"</ADDRESS>"
        data += "<STATION>"+awbagent_station+"</STATION>"
        data += "<CITY>"+awbagent_city+"</CITY>"
        data += "<STATE>"+awbagent_state+"</STATE>"
        data += "<COUNTRY>"+awbagent_country+"</COUNTRY>"
        data += "<POSTALCODE>"+awbagent_postalcode+"</POSTALCODE>"
        data += "<PHONE>"+awbagent_phone+"</PHONE>"
        data += "<FAX>"+awbagent_fax+"</FAX>"
        data += "<EMAIL>"+awbagent_email+"</EMAIL>"
        data += "</AWBAGENT>"

        const shptloadablevol = inputData.bookingrequest.shptloadablevol;
        const awbdescription = inputData.bookingrequest.awbdescription;
        data += "<SHPTLOADABLEVOL>"+shptloadablevol+"</SHPTLOADABLEVOL>"
        data += "<AWBDESCRIPTION>"+awbdescription+"</AWBDESCRIPTION>"

        const bkgsegment = inputData.bookingrequest.bkgsegment;
        for (let i = 0; i < bkgsegment.length; i++) {
            data += "<BKGSEGMENT>"
            data += "<FROM_STATION>"+bkgsegment[i].from_station+"</FROM_STATION>"
            data += "<TO_STATION>"+bkgsegment[i].to_station+"</TO_STATION>"
            data += "<FLIGHT>"+bkgsegment[i].flight+"</FLIGHT>"
            data += "<DATE>"+bkgsegment[i].date+"</DATE>"
            data += "<WEIGHT>"+bkgsegment[i].weight+"</WEIGHT>"
            data += "<VOLUME>"+bkgsegment[i].volume+"</VOLUME>"
            data += "<PIECES>"+bkgsegment[i].pieces+"</PIECES>"
            data += "<ITNRYLOADVOL>"+bkgsegment[i].itnryloadvol+"</ITNRYLOADVOL>"
            data += "</BKGSEGMENT>"
        }
       
        // data += "<BKGSEGMENT>"
        // data += "<FROM_STATION>IAD</FROM_STATION>"
        // data += "<TO_STATION>EWR</TO_STATION>"
        // data += "<FLIGHT>UA2250</FLIGHT>"
        // data += "<DATE>22May19</DATE>"
        // data += "<WEIGHT>20</WEIGHT>"
        // data += "<VOLUME>0.001</VOLUME>"
        // data += "<PIECES>1</PIECES>"
        // data += "<ITNRYLOADVOL>0.001</ITNRYLOADVOL>"
        // data += "</BKGSEGMENT>"
        const bookingremark1 = inputData.bookingrequest.bookingremark1;
        data += "<BOOKINGREMARK1>"+bookingremark1+"</BOOKINGREMARK1>"

        const dims = inputData.bookingrequest.dims;
        data += "<DIMS>"+dims+"</DIMS>"

        const source = inputData.bookingrequest.source;
        data += "<SOURCE>"+source+"</SOURCE>"
        //const ratinginfoentrycom = inputData.bookingrequest.rating-info.entry.comm
        const ratinginfoentrycom = req.body.bookingrequest["rating-info"].entry.comm;
        data += "<RATING-INFO><ENTRY><COMM>"+ratinginfoentrycom+"</COMM></ENTRY></RATING-INFO>"
        const contact_email = inputData.bookingrequest.contact.email
        const contact_phone = inputData.bookingrequest.contact.phone
        const contact_name = inputData.bookingrequest.contact.name
        data += "<CONTACT>"
        data += "<EMAIL>"+contact_email+"</EMAIL>"
        data += "<PHONE>"+contact_phone+"</PHONE>"
        data += "<NAME>"+contact_name+"</NAME>"
        data += "</CONTACT>"

        const caller = inputData.bookingrequest.caller
        data += "<CALLER>"+caller+"</CALLER>"
        data += "</BOOKINGREQUEST>";
        return data;
    }
    
}
